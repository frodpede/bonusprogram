package frodpede.ntnu.idatt2001;

public class BasicMembership extends Membership {
    private static final String MEMBERSHIP_NAME = "Basic";

    @Override
    public int registerPoints(int bonusPointBalance, int newPoints) {
        return Math.round(bonusPointBalance + newPoints);
    }
    @Override
    public String getMembershipName() {
        return MEMBERSHIP_NAME;
    }
}
