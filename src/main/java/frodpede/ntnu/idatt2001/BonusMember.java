package frodpede.ntnu.idatt2001;

import java.time.LocalDate;

/**
 * This class contain all the information about a bonus member.
 * it provides functionality for registering new points, checking if
 * a password matches the password of a bonus member, and checking witch
 * membership the bonus member have.
 *
 * @author Frode Pedersen
 */

public class BonusMember {

    private int memberNumber;
    private LocalDate enrolledDate;
    private int bonusPointsBalance = 0;
    private String name;
    private String eMailAddress;
    private String password;
    private Membership membership;

    private static final int SILVER_LIMIT = 25000;
    private static final int GOLD_LIMIT = 75000;

    /**
     *
     * @param memberNumber the member number
     * @param enrolledDate the enrolled date
     * @param bonusPointBalance the balance of the bonus points
     * @param name the name of the member
     * @param eMailAddress the email-address of the member
     */
    public BonusMember(int memberNumber, LocalDate enrolledDate, int bonusPointBalance, String name, String eMailAddress) {
        this.memberNumber = memberNumber;
        this.enrolledDate = enrolledDate;
        this.bonusPointsBalance = bonusPointBalance;
        this.name = name;
        this.eMailAddress = eMailAddress;

        checkAndSetMembership();
    }

    public int getMemberNumber() {
        return memberNumber;
    }

    public LocalDate getEnrolledDate() {
        return enrolledDate;
    }

    public int getBonusPointsBalance() {
        return bonusPointsBalance;
    }

    public String getName() {
        return name;
    }

    public String geteMailAddress() {
        return eMailAddress;
    }

    /**
     * Check if the password matches a password with a bonus member.
     * @param password
     * @return {@code true} if the password matches the password of the bonus member,
     * {@code false} if not.
     */
    public boolean checkForPassword(String password) {
        if(password == null) {
            return false;
        }
        return this.password.equals(password);
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Registers new points to a bonus member.
     * @param newPoints
     * @throws IllegalArgumentException if the points are below 0
     */

    public void registerBonusPoints(int newPoints) throws IllegalArgumentException {
        if (newPoints <= 0) {
            throw new IllegalArgumentException("New points can't be negative");
        }
        bonusPointsBalance = membership.registerPoints(bonusPointsBalance, newPoints);
        checkAndSetMembership();
    }

    /**
     * Sets the membership to the correct membership level.
     */
    public void checkAndSetMembership() {
        boolean silverMember = (bonusPointsBalance >= SILVER_LIMIT);
        boolean goldMember = (bonusPointsBalance >= GOLD_LIMIT);

        if(goldMember) {
            membership = new GoldMembership();
        } else if(silverMember) {
            membership = new SilverMembership();
        } else {
            membership = new BasicMembership();
        }
    }
    @Override
    public String toString() {
        return "BonusMember[" +
                "memberNumber=" + memberNumber +
                ", enrolledDate=" + enrolledDate +
                ", bonusPointsBalance=" + bonusPointsBalance +
                ", name='" + name + '\'' +
                ", eMailAddress='" + eMailAddress + '\'' +
                ", membership=" + membership.getMembershipName() +
                ']';
    }
}
