package frodpede.ntnu.idatt2001;

public class GoldMembership extends Membership{
    private static final String MEMBERSHIP_NAME = "Gold";
    private static final float POINT_SCALING_FACTOR = 1.5f;

    @Override
    public int registerPoints(int bonusPointBalance, int newPoints) {
        float regPoints = (bonusPointBalance + newPoints * POINT_SCALING_FACTOR);
        return Math.round(regPoints);
    }
    @Override
    public String getMembershipName() {
        return MEMBERSHIP_NAME;
    }
}
