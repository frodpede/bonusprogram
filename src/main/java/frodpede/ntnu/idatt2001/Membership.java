package frodpede.ntnu.idatt2001;

public abstract class Membership {

    public abstract int registerPoints(int bonusPointBalance, int newPoints);
    public abstract String getMembershipName();
}
